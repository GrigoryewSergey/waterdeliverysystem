namespace GeodataProvider
{
    public class PathDetails
    {
        public int Duration { get; set; }
        public int Distance { get; set; }
    }
}