﻿namespace GeodataProvider
{
    using System.Net;
    using Newtonsoft.Json;

    public class GoogleMapsProvider
    {
        public PathDetails GetPathDetails(string addressFrom, string addressTo)
        {
            var url =
                string.Format(
                @"https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&mode=driving&region=ru&language=ru-RU&sensor=false",
                    addressFrom, addressTo);

            using (var client = new WebClient())
            {
                var result = client.DownloadString(url);
                var deserializeObject = JsonConvert.DeserializeObject<RootObject>(result);

                var distance = deserializeObject.Rows[0].Elements[0].Distance.Value;
                var duration = deserializeObject.Rows[0].Elements[0].Duration.Value;

                var details = new PathDetails
                {
                    Distance = distance,
                    Duration = duration
                };

                return details;
            }
        }
    }
}