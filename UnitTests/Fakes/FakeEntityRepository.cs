﻿namespace UnitTests.Fakes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataProvider.Repositories;
    using Domain.Entities;

    public class FakeEntityRepository : IRepository
    {
        private readonly Dictionary<Type, object> sets = new Dictionary<Type, object>();
        public readonly List<object> Added = new List<object>();
        public readonly List<object> Removed = new List<object>();
        public readonly List<object> Updated = new List<object>();

        public IQueryable<TEntity> Query<TEntity>() where TEntity : BaseEntity
        {
            return sets[typeof (TEntity)] as IQueryable<TEntity>;
        }

        public void Create<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Added.Add(entity);
        }

        public void Create<TEntity>(IEnumerable<TEntity> entities) where TEntity : BaseEntity
        {
            sets.Add(typeof (TEntity), entities);
        }

        public IEnumerable<TEntity> Read<TEntity>() where TEntity : BaseEntity
        {
            return sets[typeof (TEntity)] as IEnumerable<TEntity>;
        }

        public TEntity Read<TEntity>(int entityId) where TEntity : BaseEntity
        {
            var collection = sets[typeof (TEntity)] as IQueryable<TEntity>;

            return collection != null
                ? collection.Select(entity => entity).FirstOrDefault(entiy => entiy.Id == entityId)
                : null;
        }

        public void Update<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Updated.Add(entity);
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Removed.Add(entity);
        }
    }
}