﻿namespace UnitTests.DomainUnitTests
{
    using Domain.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class OrderLineTest
    {
        private static OrderLine SetupOrderLine()
        {
            var product = new Product
            {
                Name = "Вода питьевая Aqua 1 л",
                Volume = 1,
                Price = 30
            };

            var orderLine = new OrderLine(product, 2);

            return orderLine;
        }

        [Test]
        public void AmountTest()
        {
            var orderLine = SetupOrderLine();

            Assert.AreEqual(60, orderLine.Amount);
        }

        [Test]
        public void VolumeTest()
        {
            var orderLine = SetupOrderLine();

            Assert.AreEqual(2, orderLine.Volume);
        }
    }
}