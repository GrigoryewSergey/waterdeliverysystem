﻿namespace UnitTests.DomainUnitTests
{
    using System.Collections.Generic;
    using Domain.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class OrderTest
    {
        private static Order SetupOrder()
        {
            const int product1Quantity = 2;
            const int product2Quantity = 1;

            var product1 = new Product
            {
                Name = "Вода питьевая Aqua 1 л",
                Volume = 1,
                Price = 30
            };

            var product2 = new Product
            {
                Name = "Вода питьевая Aqua 19 л",
                Volume = 19,
                Price = 570
            };

            var orderLines = new List<OrderLine>
            {
                new OrderLine(product1, product1Quantity),
                new OrderLine(product2, product2Quantity)
            };

            var order = new Order(orderLines);

            return order;
        }

        [Test]
        public void AmountTest()
        {
            var order = SetupOrder();

            Assert.AreEqual(630, order.Amount);
        }

        [Test]
        public void VolumeTest()
        {
            var order = SetupOrder();

            Assert.AreEqual(21, order.Volume);
        }
    }
}