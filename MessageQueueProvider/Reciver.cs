﻿namespace MessageQueueProvider
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataProvider.Repositories;
    using Domain.Entities;
    using GeodataProvider;
    using RabbitMQ.Client;

    public class Reciver
    {
        public void Recive()
        {
            var factory = new ConnectionFactory {HostName = "localhost"};
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare("hello", false, false, false, null);

                    var consumer = new QueueingBasicConsumer(channel);
                    channel.BasicConsume("hello", true, consumer);

                    while (true)
                    {
                        var ea = consumer.Queue.Dequeue();

                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);

                        var data = message.Split(';').ToList();

                        AddLocation(message);


                        Console.WriteLine("Address: {0}; Type: {1}", data[0], data[1]);
                    }
                }
            }
        }

        private static void AddLocation(string message)
        {
            var provider = new GoogleMapsProvider();
            var context = new DeliverySystemContext();
            
            var data = message.Split(';').ToList();

            var newLocation = new Location
            {
                Address = data[0],
                LocationType = data[1]
            };
                        
            var locations = context.Locations.Select(l => l).ToList();

            context.Locations.Add(newLocation);
            context.SaveChanges();

            var id = context.Locations.Select(l => l).FirstOrDefault(l => l.Address == newLocation.Address).Id;

            var paths = new List<Path>();

            foreach (var location in locations)
            {
                var details = provider.GetPathDetails(newLocation.Address, location.Address);

                var path = new Path
                {
                    StartId = location.Id,
                    EndId = id,
                    Distance = details.Distance,
                    Duration = details.Duration
                };

                paths.Add(path);

                Console.WriteLine("Duration: {0}; Distance: {1}", details.Duration / 60, details.Distance / 1000);
            }

            context.Paths.AddRange(paths);
            context.SaveChanges();
        }
    }
}