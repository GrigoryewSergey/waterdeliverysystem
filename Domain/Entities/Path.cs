﻿namespace Domain.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    ///     Путь между двумя месторасположениями
    /// </summary>
    public class Path : BaseEntity
    {
        /// <summary>
        ///     Внешний ключ на начальное месторасположение
        /// </summary>        
        public int StartId { get; set; }
        
        /// <summary>
        ///     Внешний ключ для конечного месторасположения
        /// </summary>        
        public int EndId { get; set; }
        
        /// <summary>
        ///     Продолжительность проезда из начального в конечное месторасположение
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        ///     Протяженность пути из начального в конечное месторасположение
        /// </summary>
        public int Distance { get; set; }
    }
}