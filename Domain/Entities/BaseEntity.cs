﻿namespace Domain.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///     Базовай класс для сущностей предметной области
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        ///     Первичный ключ для хранения сущностей в базе даных
        /// </summary>
        [Key]
        public int Id { get; set; }
    }
}