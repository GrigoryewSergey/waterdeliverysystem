﻿namespace Domain.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    ///     Заказ
    /// </summary>
    public class Order : BaseEntity
    {
        public Order()
        {
            
        }

        public Order(Location location)
        {
            Location = location;
        }

        /// <summary>
        ///     Дата доставки заказа
        ///     Хранится в виде строки вида: dd.mm.yyyy
        /// </summary>
        [Display(Name = "Дата доставки")]
        public string DeliveryDate { get; set; }

        /// <summary>
        ///     Внешний ключ на месторасположение
        /// </summary>
        [ForeignKey("Location")]
        [Display(Name = "Адрес доставки")]
        public int LocationId { get; set; }

        /// <summary>
        ///     Местораположение клиента
        /// </summary>
        [Display(Name = "Адрес доставки")]
        public Location Location { get; set; }

        /// <summary>
        ///     Общий объем заказа
        /// </summary>
        [Display(Name = "Объем заказа")]
        public double Volume { get; private set; }

        /// <summary>
        ///     Общая стоимость заказа
        /// </summary>
        [Display(Name = "Стоимость заказа")]
        public double Amount { get; private set; }


        /// <summary>
        ///     Конструктор класса
        /// </summary>
        /// <param name="orderLines">Строки заказа</param>
        public Order(IEnumerable<OrderLine> orderLines)
        {
            CalculateOrderDetails(orderLines);
        }

        /// <summary>
        ///     Расчитыавет детали заказа
        /// </summary>
        /// <param name="orderLines"></param>
        private void CalculateOrderDetails(IEnumerable<OrderLine> orderLines)
        {
            foreach (var orderLine in orderLines)
            {
                Amount += orderLine.Amount;
                Volume += orderLine.Volume;
            }
        }
    }
}