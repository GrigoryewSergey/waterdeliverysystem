﻿namespace Domain.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///     Товар
    ///     Бутыль воды
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        ///     Конструктор по умолчанию
        /// </summary>
        public Product()
        {
            Name = "";
            Volume = 0;
            Price = 0;
        }

        /// <summary>
        ///     Конструктор класса
        /// </summary>
        /// <param name="name">Наименование</param>
        /// <param name="volume">Объем</param>
        /// <param name="price">Цена</param>
        public Product(string name, double volume, double price)
        {
            Name = name;
            Volume = volume;
            Price = price;  
        }        
        
        /// <summary>
        ///     Наименование
        ///     Например: "Вода питьевая Auqa 1 л"
        /// </summary>
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        /// <summary>
        ///     Объем бутылки в литрах
        ///     Например: 0.5, 1, 1.5, 3, 5, 19
        /// </summary>
        [Display(Name = "Объем")]
        public double Volume { get; set; }

        /// <summary>
        ///     Цена за одну бутыль
        /// </summary>
        [Display(Name = "Цена")]
        public double Price { get; set; }
    }
}