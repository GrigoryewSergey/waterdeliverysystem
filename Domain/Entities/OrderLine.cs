﻿namespace Domain.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    ///     Строка заказа
    /// </summary>
    public class OrderLine : BaseEntity
    {
        /// <summary>
        ///     Внешний ключ для связанного заказа
        /// </summary>
        [ForeignKey("Order")]
        public int OrderId { get; set; }

        /// <summary>
        ///     Заказ, с которым связана текущая строка заказа
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        ///     Внешний ключ для связанного продукта
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>
        ///     Продук, связанный с текущей строкой заказа
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        ///     Количество продуктов
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        ///     Общий объем строки заказа
        /// </summary>
        public double Volume { get; private set; }

        /// <summary>
        ///     Общая стоимость строки заказа
        /// </summary>
        public double Amount { get; private set; }

        /// <summary>
        ///     Конструктор по умолчанию
        /// </summary>
        public OrderLine()
        {
            Product = null;
            Order = null;
            Quantity = 0;
            Volume = 0;
            Amount = 0;
        }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="product">Связанный товар</param>
        /// <param name="quantity">Количество товаров</param>
        public OrderLine(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;

            CalculateOrderLineDetails();
        }

        /// <summary>
        ///     Расчитывает детали строки заказа
        /// </summary>
        private void CalculateOrderLineDetails()
        {
            Volume = Product.Volume * Quantity;
            Amount = Product.Price * Quantity;
        }
    }
}