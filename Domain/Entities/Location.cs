﻿namespace Domain.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///     Месторасположение на карте склада или клиента
    /// </summary>
    public class Location : BaseEntity
    {
        /// <summary>
        ///     Адрес месторасположения
        ///     Записывается в формате: НазваниеГорода, ул. НазваниеУлицы, НомерДома
        /// </summary>
        [Display(Name = "Адрес месторасположения")]
        public string Address { get; set; }

        /// <summary>
        ///     Тип месторасположения
        ///     Записывается: "Клиент" или "Склад"
        /// </summary>
        [Display(Name = "Тип месторасположения")]
        public string LocationType { get; set; }
    }
}