﻿namespace Domain.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///     Автомобиль доставки
    /// </summary>
    public class Auto : BaseEntity
    {
        /// <summary>
        ///     Водитель (полное имя)
        /// </summary>
        [Display(Name = "Водитель")]
        public string Driver { get; set; }

        /// <summary>
        ///     Название автомобиля
        /// </summary>
        [Display(Name = "Марка автомобиля")]
        public string Name { get; set; }

        /// <summary>
        ///     Государственный решистрационный номер
        /// </summary>
        [Display(Name = "Регистрационный номер")]
        public string RegistrationNumber { get; set; }

        /// <summary>
        ///     Общий объем фургона (в литрах)
        /// </summary>
        [Display(Name = "Объем")]
        public double Volume { get; set; }
    }
}