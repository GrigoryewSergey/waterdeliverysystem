﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Domain")]
[assembly: AssemblyDescription("Library contains domain entities")]
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyCompany("Water Delivery System")]
[assembly: AssemblyProduct("Domain")]
[assembly: AssemblyCopyright("Copyright ©  WaterDeliverySystem")]
[assembly: AssemblyTrademark("Water Delivery System")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("ce03e58d-111d-4dac-866b-0fcdb722eaec")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]