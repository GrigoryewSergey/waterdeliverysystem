﻿namespace DataProvider.Repositories
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Domain.Entities;

    /// <summary>
    ///     Реализация репозитория для доступа к базе данных
    /// </summary>
    public class EntityRepository : DbContext, IRepository
    {
        public IQueryable<TEntity> Query<TEntity>() where TEntity : BaseEntity
        {
            return Set<TEntity>();
        }

        public void Create<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Set<TEntity>().Add(entity);
            SaveChanges();
        }

        public void Create<TEntity>(IEnumerable<TEntity> entities) where TEntity : BaseEntity
        {
            Set<TEntity>().AddRange(entities);
            SaveChanges();
        }

        public IEnumerable<TEntity> Read<TEntity>() where TEntity : BaseEntity
        {
            return Set<TEntity>();
        }

        public TEntity Read<TEntity>(int entityId) where TEntity : BaseEntity
        {
            return Set<TEntity>().Select(entity => entity).FirstOrDefault(entity => entity.Id == entityId);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Entry(entity).State = EntityState.Modified;
            SaveChanges();
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            Set<TEntity>().Remove(entity);
        }
    }
}