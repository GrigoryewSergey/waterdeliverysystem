﻿namespace DataProvider.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Domain.Entities;

    /// <summary>
    ///     CRUD репозиторий для доступа к базе данных
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        ///     Запрос к коллекции элементов
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <returns>Коллекция сущнотей</returns>
        IQueryable<TEntity> Query<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        ///     Создание новой сущности в базе данных
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <param name="entity">Создаваяемая сущность</param>
        void Create<TEntity>(TEntity entity) where TEntity : BaseEntity;

        /// <summary>
        ///     Создание коллекции новых сущностей
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <param name="entities">Коллекция сущностей</param>
        void Create<TEntity>(IEnumerable<TEntity> entities) where TEntity : BaseEntity;

        /// <summary>
        ///     Получить все коллекцию сущностей
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <returns>Коллекция сущностей</returns>
        IEnumerable<TEntity> Read<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        ///     Получить сущность по идентификатору
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <param name="entityId">Идентификатор сущности</param>
        /// <returns>Сущность по идентификатору</returns>
        TEntity Read<TEntity>(int entityId) where TEntity : BaseEntity;

        /// <summary>
        ///     Обновить сущность
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <param name="entity">Обновленная сущность</param>
        void Update<TEntity>(TEntity entity) where TEntity : BaseEntity;

        /// <summary>
        ///     Удалить сущность
        /// </summary>
        /// <typeparam name="TEntity">Сущность в базе данных</typeparam>
        /// <param name="entity">Удаляемая сущность</param>
        void Delete<TEntity>(TEntity entity) where TEntity : BaseEntity;
    }
}