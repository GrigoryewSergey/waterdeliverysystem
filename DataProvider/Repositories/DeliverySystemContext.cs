﻿namespace DataProvider.Repositories
{
    using System.Data.Entity;
    using Domain.Entities;

    public class DeliverySystemContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public DeliverySystemContext() : base("name=DeliverySystemContext")
        {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Auto> Autoes { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderLine> OrderLines { get; set; }

        public DbSet<Path> Paths { get; set; }
    }
}