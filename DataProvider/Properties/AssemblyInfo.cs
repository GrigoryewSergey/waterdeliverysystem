﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DataProvider")]
[assembly: AssemblyDescription("Data base provider library")]
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyCompany("Water Delivery System")]
[assembly: AssemblyProduct("DataProvider")]
[assembly: AssemblyCopyright("Copyright ©  Water Delivery System")]
[assembly: AssemblyTrademark("Water Delivery System")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("b4fe5aad-7ec9-430d-851e-10f1f170df07")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]