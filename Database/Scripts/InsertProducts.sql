SET IDENTITY_INSERT [dbo].[Products] ON
INSERT INTO [dbo].[Products] ([Id], [Name], [Volume], [Price]) VALUES (1, N'Вода питьевая Премиум Аква 1 л', 1, 30)
INSERT INTO [dbo].[Products] ([Id], [Name], [Volume], [Price]) VALUES (2, N'Вода питьевая Премиум Аква 5 л', 5, 150)
INSERT INTO [dbo].[Products] ([Id], [Name], [Volume], [Price]) VALUES (3, N'Вода питьевая Премиум Аква 19 л', 19, 570)
SET IDENTITY_INSERT [dbo].[Products] OFF
