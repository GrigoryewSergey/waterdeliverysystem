CREATE TABLE [dbo].[Products] (
    [Id]     INT            IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (MAX) NULL,
    [Volume] FLOAT (53)     NOT NULL,
    [Price]  FLOAT (53)     NOT NULL,
    CONSTRAINT [PK_dbo.Products] PRIMARY KEY CLUSTERED ([Id] ASC)
);