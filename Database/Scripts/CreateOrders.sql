CREATE TABLE [dbo].[Orders] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [DeliveryDate] NVARCHAR (MAX) NULL,
    [LocationId]   INT            NOT NULL,
    [Volume]       FLOAT (53)     NOT NULL,
    [Amount]       FLOAT (53)     NOT NULL,
    CONSTRAINT [PK_dbo.Orders] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Orders_dbo.Locations_LocationId] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Locations] ([Id]) ON DELETE CASCADE
);