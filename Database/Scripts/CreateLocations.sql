CREATE TABLE [dbo].[Locations] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Address]      NVARCHAR (MAX) NULL,
    [LocationType] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Locations] PRIMARY KEY CLUSTERED ([Id] ASC)
);