CREATE TABLE [dbo].[Paths] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [StartId]  INT NOT NULL,
    [EndId]    INT NOT NULL,
    [Duration] INT NOT NULL,
    [Distance] INT NOT NULL,
    CONSTRAINT [PK_dbo.Paths] PRIMARY KEY CLUSTERED ([Id] ASC)
);