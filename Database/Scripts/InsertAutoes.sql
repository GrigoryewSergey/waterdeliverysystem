SET IDENTITY_INSERT [dbo].[Autoes] ON
INSERT INTO [dbo].[Autoes] ([Id], [Driver], [Name], [RegistrationNumber], [Volume]) VALUES (1, N'Демьянов Родион Олегович', N'Газель Фургон', N'с189мк74RUS', 8000)
INSERT INTO [dbo].[Autoes] ([Id], [Driver], [Name], [RegistrationNumber], [Volume]) VALUES (2, N'Александров Олег Семенович', N'Газель Фургон', N'в123ек74RUS', 8000)
INSERT INTO [dbo].[Autoes] ([Id], [Driver], [Name], [RegistrationNumber], [Volume]) VALUES (3, N'Леонтьев Семён Станиславович', N'Газель Фургон', N'к565рс74RUS', 9000)
INSERT INTO [dbo].[Autoes] ([Id], [Driver], [Name], [RegistrationNumber], [Volume]) VALUES (4, N'Пчёлкин Леонид Петрович', N'Газель Фургон', N'в729кк74RUS', 9000)
SET IDENTITY_INSERT [dbo].[Autoes] OFF
