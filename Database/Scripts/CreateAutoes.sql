CREATE TABLE [dbo].[Autoes] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [Driver]             NVARCHAR (MAX) NULL,
    [Name]               NVARCHAR (MAX) NULL,
    [RegistrationNumber] NVARCHAR (MAX) NULL,
    [Volume]             FLOAT (53)     NOT NULL,
    CONSTRAINT [PK_dbo.Autoes] PRIMARY KEY CLUSTERED ([Id] ASC)
);