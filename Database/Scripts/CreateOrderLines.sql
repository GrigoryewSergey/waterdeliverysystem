CREATE TABLE [dbo].[OrderLines] (
    [Id]        INT        IDENTITY (1, 1) NOT NULL,
    [OrderId]   INT        NOT NULL,
    [ProductId] INT        NOT NULL,
    [Quantity]  INT        NOT NULL,
    [Volume]    FLOAT (53) NOT NULL,
    [Amount]    FLOAT (53) NOT NULL,
    CONSTRAINT [PK_dbo.OrderLines] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.OrderLines_dbo.Orders_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.OrderLines_dbo.Products_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([Id]) ON DELETE CASCADE
);